//
//  RecoveryConnectView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/18/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct RecoveryConnectView: View {
    @Binding var currentView: L1ghtningView
    
    var body: some View {
        VStack(spacing: 50) {
            VStack {
                Text("L1ghtning")
                    .font(.largeTitle)
                Text("Connect via recovery mode")
                    .font(.headline)
            }
            
            Text("Connect your device in normal, recovery, or DFU mode to your computer with a Lightning to USB-A cable. In this beta, you must manually enter pwned recovery mode and install libirecovery.")
                .multilineTextAlignment(.center)
            
            HStack {
                Button(action: {
                    self.currentView = .connect
                }) {
                    Text("Back")
                }
                
                Button(action: {
                    Connection.shared.useRecoveryModeInstead = true
                    self.currentView = .home
                }) {
                    Text("Next")
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct RecoveryConnectView_Previews: PreviewProvider {
    static var previews: some View {
        RecoveryConnectView(currentView: .constant(.recoveryConnect))
    }
}
