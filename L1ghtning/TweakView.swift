//
//  TweakView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/29/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct TweakView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var tweaksLoaded = false
    @State private var tweakProgress: CGFloat = 0
    @State private var tweaks = [Tweak]()
    @State private var removeAlert = false
    @State private var removeAlertView = Alert(
        title: Text("Hey There"),
        message: Text("Hey there! If you see this, you just found a bug. That's embarrassing :/")
    )
    
    var body: some View {
        VStack(spacing: 50) {
            Text("Tweaks")
                .niceText(font: .largeTitle)
            
            if tweaksLoaded {
                GeometryReader { geometry in
                    ScrollView {
                        VStack {
                            ForEach(self.tweaks) { tweak in
                                HStack(spacing: 20) {
                                    Text(tweak.name ?? tweak.identifier)
                                        .frame(width: 300)
                                        .niceText()
                                    
                                    Button(action: {
                                        DispatchQueue.global(qos: .background).async {
                                            self.removeAlertView = Alert(
                                                title: Text("Remove Tweak"),
                                                message: Text("Are you sure you want to remove \(tweak.name ?? tweak.identifier)?"),
                                                primaryButton: .cancel(Text("No")),
                                                secondaryButton: .destructive(Text("Yes")) {
                                                    let error = Connection.shared.run(command: "dpkg -r \(tweak.identifier) && killall SpringBoard")
                                                    if error != nil {
                                                        self.removeAlertView = Alert(
                                                            title: Text("Failed to Remove Tweak"),
                                                            message: Text("An error occurred while trying to remove \(tweak.name ?? tweak.identifier).")
                                                        )
                                                        self.removeAlert = true
                                                    }
                                                    self.loadTweaks()
                                                }
                                            )
                                            self.removeAlert = true
                                        }
                                    }) {
                                        Text("Remove")
                                            .niceButtonText()
                                    }
                                    .niceButton(color: .red)
                                    .alert(isPresented: self.$removeAlert) {
                                        self.removeAlertView
                                    }
                                }
                            }
                        }.frame(width: geometry.size.width)
                    }
                }
            } else {
                VStack {
                    Text("Loading tweaks...")
                        .niceText()
                    ProgressBar(progress: $tweakProgress)
                    Text("Better (and quicker) tweak loading will be coming in the next beta.")
                        .foregroundColor(.red)
                        .niceText()
                }
            }
            
            HStack {
                Button(action: {
                    self.currentView = .home
                }) {
                    Text("Back")
                        .niceButtonText()
                }.niceButton(preset: .bottomButton)
                
                /*
                if tweaksLoaded {
                    Button(action: {}) {
                        Text("Install DEB")
                            .niceButtonText()
                    }.niceButton(color: .green, preset: .bottomButton)
                }
                */
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
        .onAppear(perform: loadTweaks)
    }
    
    func loadTweaks() {
        if tweaksLoaded {
            return
        }
        
        tweaksLoaded = false
        tweakProgress = 0
        tweaks = []
        
        let identifiers = Connection.shared.deviceInfo?.tweaks ?? []
        DispatchQueue.global(qos: .background).async {
            identifiers.forEach { identifier in
                let (error, output) = Connection.shared.runWithOutput(command: "apt show \(identifier) | grep 'Name: '")
                
                if error != nil {
                    self.tweaks.append(Tweak(identifier: identifier))
                } else {
                    let name = output?
                        .components(separatedBy: "Name: ")[1]
                        .components(separatedBy: .newlines)[0]
                    self.tweaks.append(Tweak(
                        identifier: identifier,
                        name: name
                    ))
                }
                
                self.tweakProgress = (CGFloat(self.tweaks.count) / CGFloat(identifiers.count)) * 100
                if self.tweaks.count == identifiers.count {
                    self.tweaksLoaded = true
                }
            }
            
            self.tweaks.sort {
                ($0.name ?? $0.identifier) < ($1.name ?? $1.identifier)
            }
        }
    }
}

struct TweakView_Previews: PreviewProvider {
    static var previews: some View {
        TweakView(currentView: .constant(.tweak))
    }
}

struct Tweak: Identifiable {
    let id = UUID()
    let identifier: String
    var name: String? = nil
}
