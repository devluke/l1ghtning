//
//  InfoView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/27/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct InfoView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var hideSensitive = true
    
    var body: some View {
        VStack(spacing: 50) {
            Text("Info")
                .niceText(font: .largeTitle)
            
            if Connection.shared.deviceInfo != nil {
                HStack(alignment: .top, spacing: 50) {
                    VStack(spacing: 20) {
                        Text("Basic Info")
                            .niceText(font: .headline)
                        
                        VStack {
                            Text("Identifier")
                                .niceText(font: .subheadline)
                            Text(Connection.shared.deviceInfo!.identifier)
                                .niceText()
                        }
                        VStack {
                            Text("Type")
                                .niceText(font: .subheadline)
                            Text(Connection.shared.deviceInfo!.type)
                                .niceText()
                        }
                        VStack {
                            Text("Version")
                                .niceText(font: .subheadline)
                            Text(Connection.shared.deviceInfo!.version)
                                .niceText()
                        }
                    }
                    
                    VStack(spacing: 20) {
                        Text("IP Address")
                            .niceText(font: .headline)
                        
                        VStack {
                            Text("Local")
                                .niceText(font: .subheadline)
                            Text(Connection.shared.deviceInfo!.localIp)
                                .niceText()
                        }
                        VStack {
                            Text("Public")
                                .niceText(font: .subheadline)
                            if !hideSensitive {
                                Text(Connection.shared.deviceInfo!.publicIp)
                                    .niceText()
                            } else {
                                Text("(hidden)")
                                    .niceText()
                            }
                        }
                    }
                }
            } else {
                Text("Failed to load device info.")
                    .niceText(font: .subheadline)
            }
            
            HStack {
                Button(action: {
                    self.currentView = .home
                }) {
                    Text("Back")
                        .niceButtonText()
                }.niceButton(preset: .bottomButton)
                
                Button(action: {
                    self.hideSensitive.toggle()
                }) {
                    Text("\(self.hideSensitive ? "Show" : "Hide") Sensitive Info")
                        .niceButtonText()
                }.niceButton(color: .gray, preset: .bottomButton)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView(currentView: .constant(.info))
    }
}
