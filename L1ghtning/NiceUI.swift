//
//  NiceUI.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/28/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct NiceUI: View {
    static var use = true
    
    @State private var textField = ""
    @State private var secureField = "abc123"
    
    var body: some View {
        VStack(spacing: 30) {
            HStack(alignment: .top, spacing: 30) {
                VStack(spacing: 10) {
                    Button(action: {}) {
                        Text("Test Button")
                            .niceButtonText()
                    }.niceButton()
                    
                    Button(action: {}) {
                        Text("Another Test Button")
                            .niceButtonText()
                    }.niceButton(color: .red, corner: 5)
                }
                
                Button(action: {}) {
                    Text("Long Button")
                        .niceButtonText()
                }.niceButton(corner: 10, height: 70)
            }
            
            VStack {
                Text("Large Title")
                    .niceText(font: .largeTitle)
                Text("Headline")
                    .niceText(font: .headline)
                Text("And finally, this is regular text.")
                    .niceText()
            }
            
            VStack {
                TextField(" ", text: $textField)
                    .niceField(placeholder: "Test Text Field", text: $textField)
                SecureField(" ", text: $secureField)
                    .niceField(placeholder: "Test Secure Field", text: $secureField)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct NiceUI_Previews: PreviewProvider {
    static var previews: some View {
        NiceUI()
    }
}

class Nice {
    struct Text: ViewModifier {
        var font: OpenSansFont = .regular
        
        func body(content: Content) -> some View {
            ZStack {
                if NiceUI.use {
                    content
                        .font(.custom("Open Sans", size: font.rawValue))
                } else {
                    content
                        .font(.system(size: font.rawValue))
                }
            }
        }
    }

    struct ButtonText: ViewModifier {
        func body(content: Content) -> some View {
            ZStack {
                if NiceUI.use {
                    content
                        .foregroundColor(.white)
                        .padding()
                        .font(.custom("Open Sans", size: 14))
                } else {
                    content
                }
            }
        }
    }

    struct Button: ViewModifier {
        var color: Color? = nil
        var corner: CGFloat? = nil
        var height: CGFloat? = nil
        
        var preset: ButtonPreset? = nil
        
        func body(content: Content) -> some View {
            ZStack {
                if NiceUI.use {
                    content
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .buttonStyle(PlainButtonStyle())
                        .frame(height: height ?? (preset?.height ?? 30))
                        .background(
                            RoundedRectangle(cornerRadius: corner ?? (preset?.corner ?? 40))
                                .foregroundColor(color ?? (preset?.color ?? .blue))
                        )
                } else {
                    content
                }
            }
        }
    }
    
    struct Field: ViewModifier {
        let placeholder: String
        @Binding var text: String
        
        func body(content: Content) -> some View {
            ZStack {
                if NiceUI.use {
                    content
                    /*
                    content
                        .textFieldStyle(PlainTextFieldStyle())
                        .font(.custom("Open Sans", size: 13))
                        .padding(.horizontal, 10)
                        .padding(.vertical, 5)
                        .background(
                            RoundedRectangle(cornerRadius: 40)
                                .foregroundColor(.white)
                        )
                    if text.isEmpty {
                        HStack {
                            SwiftUI.Text(placeholder)
                                .foregroundColor(.gray)
                                .font(.custom("Open Sans", size: 13))
                                .padding(.horizontal, 10)
                                .padding(.vertical, 5)
                            Spacer()
                        }
                    }
                    */
                } else {
                    content
                }
            }
        }
    }
}

extension View {
    func niceText(font: OpenSansFont = .regular) -> some View {
        self.modifier(Nice.Text(font: font))
    }
    
    func niceButtonText() -> some View {
        self.modifier(Nice.ButtonText())
    }
    
    func niceButton(color: Color? = nil, corner: CGFloat? = nil, height: CGFloat? = nil, preset: ButtonPreset? = nil) -> some View {
        self.modifier(Nice.Button(color: color, corner: corner, height: height, preset: preset))
    }
    
    func niceField(placeholder: String, text: Binding<String>) -> some View {
        self.modifier(Nice.Field(placeholder: placeholder, text: text))
    }
}

struct ButtonPreset {
    var color: Color = .blue
    var corner: CGFloat = 40
    var height: CGFloat = 30
    
    static let bottomButton = ButtonPreset(corner: 5)
}

enum OpenSansFont: CGFloat {
    case largeTitle = 31
    case title = 26
    case headline = 17
    case subheadline = 15
    case regular = 13
}
