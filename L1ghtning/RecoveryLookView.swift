//
//  RecoveryLookView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/24/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct RecoveryLookView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var red = ""
    @State private var green = ""
    @State private var blue = ""
    @State private var alpha = ""
    @State private var animation = 0
    
    var body: some View {
        VStack(spacing: 50) {
            Text("Looks")
                .font(.largeTitle)
            
            VStack {
                Text("Animation")
                    .font(.subheadline)
                Picker(selection: $animation, label: EmptyView()) {
                    Text("None").tag(0)
                    Text("Fade").tag(1)
                    Text("Pulse").tag(2)
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            HStack(spacing: 50) {
                VStack {
                    TextField("0", text: $red)
                        .multilineTextAlignment(.center)
                    Text("Red")
                }
                VStack {
                    TextField("153", text: $red)
                        .multilineTextAlignment(.center)
                    Text("Green")
                }
                VStack {
                    TextField("255", text: $red)
                        .multilineTextAlignment(.center)
                    Text("Blue")
                }
                VStack {
                    TextField("1", text: $red)
                        .multilineTextAlignment(.center)
                    Text("Alpha")
                }
            }
            
            Button(action: {
                DispatchQueue.global(qos: .background).async {
                    while true {
                        Ra1nbowColor.shared.pulse(color: FakeColor(red: 0, green: 153, blue: 255))
                    }
                }
            }) {
                Text("Test")
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct RecoveryLookView_Previews: PreviewProvider {
    static var previews: some View {
        RecoveryLookView(currentView: .constant(.recoveryLook))
    }
}
