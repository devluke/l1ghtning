//
//  TemplateView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/18/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct TemplateView: View { // Change to your view name
    @Binding var currentView: L1ghtningView
    
    var body: some View {
        VStack {
            Text("Hello World!")
                .niceText(font: .largeTitle)
            Text("This is a template view.")
                .niceText(font: .headline)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct TemplateView_Previews: PreviewProvider { // Change to your view name
    static var previews: some View {
        TemplateView(currentView: .constant(.home)) // Change to your view
    }
}
