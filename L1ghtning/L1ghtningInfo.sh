#!/bin/sh

#  L1ghtningInfo.sh
#  L1ghtning
#
#  Created by Luke Chambers on 11/26/19.
#  Copyright © 2019 Luke Chambers. All rights reserved.

echo
echo "If you see this, you're manually running this script, that's cool. Just so you know, the format this script prints isn't pretty, it's just something that L1ghtning can read easily and quickly. All output from this script is parsed and displayed in a human-readable format in the Info section of the L1ghting macOS/iOS apps. :)"
echo

identifier=$(uname -m)
version=$(sw_vers -productVersion)
local_ip=$(ipconfig getifaddr en0)
public_ip=$(wget -qO- icanhazip.com)

tweaks=$(apt-mark showmanual *.*.* | tr '\n' ',')
tweaks=${tweaks::-1}

echo "Info: +|identifier+:$identifier+,version+:$version+,local_ip+:$local_ip+,public_ip+:$public_ip+,tweaks+:$tweaks+|"
echo
