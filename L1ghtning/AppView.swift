//
//  AppView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/23/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct AppView: View {
    @Binding var currentView: L1ghtningView
    
    var body: some View {
        VStack(spacing: 30) {
            VStack(alignment: .leading, spacing: 30) {
                VStack(alignment: .leading) {
                    Text("L1ghtning iOS App")
                        .niceText(font: .largeTitle)
                    Text("Use parts of L1ghtning without a computer")
                        .niceText(font: .headline)
                }
                
                VStack(alignment: .leading) {
                    Text("\u{2022}  Install and remove tweaks without a package manager")
                        .niceText()
                    Text("\u{2022}  Respring, enter safe mode, reboot, or UICache")
                        .niceText()
                    Text("\u{2022}  Get detailed information about your device")
                        .niceText()
                    Text("\u{2022}  Fix Cydia crashes or uninstall Cydia")
                        .niceText()
                    Text("\u{2022}  ...and much more!")
                        .niceText()
                }
            }
            
            HStack {
                Button(action: {
                    self.currentView = .home
                }) {
                    Text("Back")
                        .niceButtonText()
                }.niceButton(preset: .bottomButton)
                
                Button(action: {}) {
                    Text("Coming Soon")
                        .niceButtonText()
                }
                .disabled(true)
                .niceButton(color: .green, preset: .bottomButton)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView(currentView: .constant(.app))
    }
}
