//
//  HomeView.swift
//  ReRa1n
//
//  Created by Luke Chambers on 11/15/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    @Binding var currentView: L1ghtningView
    
    var body: some View {
        VStack(spacing: 50) {
            VStack {
                Text("L1ghtning")
                    .niceText(font: .largeTitle)
                if Connection.shared.deviceInfo != nil {
                    Text(Connection.shared.deviceInfo!.string)
                        .niceText(font: .headline)
                }
            }
            
            HStack(spacing: 50) {
                if !Connection.shared.useRecoveryModeInstead {
                    HStack {
                        Button(action: {
                            self.currentView = .recover
                        }) {
                            Text("Recovery")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {
                            self.currentView = .utility
                        }) {
                            Text("Utilities")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {
                            self.currentView = .app
                        }) {
                            Text("App")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {
                            self.currentView = .info
                        }) {
                            Text("Info")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {
                            self.currentView = .tweak
                        }) {
                            Text("Tweaks")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                    }
                } else {
                    HStack {
                        Button(action: {}) {
                            Text("Boot")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(!Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {}) {
                            Text("Restore")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(!Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                        
                        Button(action: {
                            self.currentView = .recoveryLook
                        }) {
                            Text("Looks")
                                .frame(width: 75)
                                .niceButtonText()
                        }
                        .disabled(!Connection.shared.useRecoveryModeInstead)
                        .niceButton()
                    }
                }
            }
            
            Button(action: {
                Connection.reset()
                self.currentView = .connect
                UtilityView.showDestruction = false
            }) {
                Text("Disconnect")
                    .niceButtonText()
            }.niceButton(color: .red, preset: .bottomButton)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(currentView: .constant(.home))
    }
}
