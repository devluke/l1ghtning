//
//  ActionList.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/17/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct ActionList: View {
    let columns: [ListColumn]
    
    @State private var alert = false
    @State private var alertView = Alert(
        title: Text("Well Hello There"),
        message: Text("You probably shouldn't be seeing this alert but if you are, hello there! (This is a bug)")
    )
    
    var body: some View {
        HStack(alignment: .top, spacing: 50) {
            ForEach(columns) { column in
                if column.condition {
                    VStack(alignment: column.alignment) {
                        Text(column.title)
                            .niceText(font: .subheadline)
                        ForEach(column.actions) { action in
                            Button(action: {
                                DispatchQueue.global(qos: .background).async {
                                    func runIt() {
                                        if let runAction = action.action {
                                            runAction(self.$alert, self.$alertView)
                                        }
                                        
                                        if let command = action.command {
                                            let error = Connection.shared.run(command: command)
                                            if error != nil {
                                                self.alertView = Alert(
                                                    title: Text("Failed to Run Action"),
                                                    message: Text("An error occurred while attempting to \(action.label).")
                                                )
                                                self.alert = true
                                            }
                                        }
                                    }
                                    
                                    if action.confirm {
                                        self.alertView = Alert(
                                            title: Text("Confirm"),
                                            message: Text(action.confirmMessage),
                                            primaryButton: .cancel(Text("No")),
                                            secondaryButton: .destructive(Text("Yes")) {
                                                runIt()
                                            }
                                        )
                                        self.alert = true
                                    } else {
                                        runIt()
                                    }
                                }
                            }) {
                                Text(action.label)
                                    .niceButtonText()
                            }.alert(isPresented: self.$alert) {
                                self.alertView
                            }.niceButton(color: .gray)
                        }
                    }
                }
            }
        }
    }
}

struct ActionList_Previews: PreviewProvider {
    static var previews: some View {
        ActionList(columns: [
            ListColumn(actions: [
                ListAction(label: "Action One"),
                ListAction(label: "Action Two"),
                ListAction(label: "Action Three"),
                ListAction(label: "Action Four"),
            ], alignment: .leading),
            ListColumn(actions: [
                ListAction(label: "Action Five"),
                ListAction(label: "Action Six"),
                ListAction(label: "Action Seven"),
                ListAction(label: "Action Eight"),
            ], alignment: .center, title: "Center"),
            ListColumn(actions: [
                ListAction(label: "Action Nine"),
                ListAction(label: "Action Ten"),
                ListAction(label: "Action Eleven"),
                ListAction(label: "Action Twelve"),
            ], alignment: .trailing, title: "Trailing")
        ])
    }
}

struct ListColumn: Identifiable {
    let id = UUID()
    let actions: [ListAction]
    var alignment: HorizontalAlignment = .center
    var title = " "
    var condition = true
}

struct ListAction: Identifiable {
    let id = UUID()
    let label: String
    var confirm = false
    var confirmMessage = "Are you sure you want to run this action?"
    var action: ((Binding<Bool>, Binding<Alert>) -> Void)? = nil
    var command: Command? = nil
}
