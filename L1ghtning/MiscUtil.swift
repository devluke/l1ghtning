//
//  MiscUtil.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/24/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation

extension Int {
    func times(_ f: () -> ()) {
        if self > 0 {
            for _ in 0..<self {
                f()
            }
        }
    }
}
