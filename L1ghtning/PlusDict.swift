//
//  PlusDict.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/29/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation

class PlusDict {
    var dict = [String: String]()
    
    init(_ input: String?) {
        let plusString = input?.components(separatedBy: "+|")[1]
        plusString?.components(separatedBy: "+,").forEach { pair in
            let pairParts = pair.components(separatedBy: "+:")
            dict[pairParts[0]] = pairParts[1]
        }
    }
    
    func string(for key: String) -> String? {
        return dict[key]
    }
}
