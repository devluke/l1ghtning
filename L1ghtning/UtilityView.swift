//
//  UtilityView.swift
//  ReRa1n
//
//  Created by Luke Chambers on 11/15/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct UtilityView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var destructionMode = false
    
    static var showDestruction = false
    
    var body: some View {
        VStack(spacing: 50) {
            Text("Utilities")
                .niceText(font: .largeTitle)
            
            ActionList(columns: [
                ListColumn(actions: [
                    ListAction(label: "Respring", command: .respring),
                    ListAction(label: "Safe Mode", command: .safeMode),
                    ListAction(
                        label: "Reboot",
                        confirm: true,
                        confirmMessage: "This action will reboot your device and you'll have to rejailbreak. Are you sure you want to reboot?",
                        command: .reboot),
                    ListAction(label: "UICache", command: .uicache)
                ], title: "Basic"),
                ListColumn(actions: [
                    ListAction(
                        label: "rm -rf /",
                        confirm: true,
                        confirmMessage: "This action is /very/ destructive and can lead to a bricked device. You are the only one responsible for any damage done to your device. Are you absolutely sure you want to do this?",
                        action: { _, _ in
                            let _ = Connection.shared.run(command: .destroySystem)
                        }
                    )
                ], title: "Destruction", condition: destructionMode)
            ])
            
            VStack {
                HStack {
                    Button(action: {
                        self.currentView = .home
                    }) {
                        Text("Back")
                            .niceButtonText()
                    }.niceButton(preset: .bottomButton)
                    
                    if Self.showDestruction {
                        Button(action: {
                            self.destructionMode.toggle()
                        }) {
                            Text("Destruction Mode")
                                .niceButtonText()
                        }.niceButton(color: .red, preset: .bottomButton)
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct UtilityView_Previews: PreviewProvider {
    static var previews: some View {
        UtilityView(currentView: .constant(.utility))
    }
}
