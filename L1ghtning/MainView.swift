//
//  MainView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/16/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct MainView: View {
    @State private var currentView: L1ghtningView = .connect
    
    var body: some View {
        ZStack {
            if currentView == .depend {
                DependView(currentView: $currentView)
            } else if currentView == .connect {
                ConnectView(currentView: $currentView)
            } else if currentView == .home {
                HomeView(currentView: $currentView)
            } else if currentView == .app {
                AppView(currentView: $currentView)
            } else if currentView == .info {
                InfoView(currentView: $currentView)
            } else if currentView == .recover {
                RecoverView(currentView: $currentView)
            } else if currentView == .recoveryConnect {
                RecoveryConnectView(currentView: $currentView)
            } else if currentView == .recoveryLook {
                RecoveryLookView(currentView: $currentView)
            } else if currentView == .tweak {
                TweakView(currentView: $currentView)
            } else if currentView == .utility {
                UtilityView(currentView: $currentView)
            } else {
                Text("Failed to load view.")
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

enum L1ghtningView {
    case depend
    case connect
    case home
    case app
    case info
    case recover
    case recoveryConnect
    case recoveryLook
    case tweak
    case utility
}
