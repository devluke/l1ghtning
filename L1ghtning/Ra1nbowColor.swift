//
//  Ra1nbowColor.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/24/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftShell

class Ra1nbowColor {
    static let shared = Ra1nbowColor()
    private init() {}
    
    var red: Float = 0
    var green: Float = 0
    var blue: Float = 0
    var alpha: Float = 1
    
    func reset() {
        set(to: .black)
    }
    
    func set(to color: FakeColor) {
        let real = color.realRgb()
        "irecovery -c 'bgcolor \(real.red) \(real.green) \(real.blue)'".runCommandHackilyButSwiftly()
        red = color.red
        green = color.green
        blue = color.blue
        alpha = color.alpha
    }
    
    func fade(to color: FakeColor, frames: Int = 50) {
        let floatFrames = Float(frames)
        
        let addRed = (color.red - red) / floatFrames
        let addGreen = (color.green - green) / floatFrames
        let addBlue = (color.blue - blue) / floatFrames
        let addAlpha = (color.alpha - alpha) / floatFrames
        
        frames.times {
            let newRed = red + addRed
            let newGreen = green + addGreen
            let newBlue = blue + addBlue
            let newAlpha = alpha + addAlpha
            set(to: FakeColor(red: newRed, green: newGreen, blue: newBlue, alpha: newAlpha))
        }
        
        set(to: color)
    }
    
    func pulse(color: FakeColor, frames: Int = 50) {
        let oldRed = red
        let oldGreen = green
        let oldBlue = blue
        let oldAlpha = alpha
        fade(to: color, frames: frames)
        fade(to: FakeColor(red: oldRed, green: oldGreen, blue: oldBlue, alpha: oldAlpha))
    }
}

struct FakeColor {
    let red: Float
    let green: Float
    let blue: Float
    var alpha: Float = 1
    
    func realRgb() -> (red: Float, green: Float, blue: Float) {
        if alpha == 0 {
            return (red: 0, green: 0, blue: 0)
        }
        
        let realRed = red * alpha
        let realGreen = green * alpha
        let realBlue = blue * alpha
        
        return (red: realRed, green: realGreen, blue: realBlue)
    }
    
    static let black = FakeColor(red: 0, green: 0, blue: 0)
    static let white = FakeColor(red: 255, green: 255, blue: 255)
    static let red = FakeColor(red: 255, green: 0, blue: 0)
    static let orange = FakeColor(red: 255, green: 127, blue: 0)
    static let yellow = FakeColor(red: 255, green: 255, blue: 0)
    static let green = FakeColor(red: 0, green: 255, blue: 0)
    static let blue = FakeColor(red: 0, green: 0, blue: 255)
    static let indigo = FakeColor(red: 75, green: 0, blue: 130)
    static let violet = FakeColor(red: 139, green: 0, blue: 255)
}

enum ColorAnimation {
    case none
    case fade
    case pulse
}
