//
//  DependView.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/21/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI
import SwiftShell

struct DependView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var errorAlert = false
    @State var progress: CGFloat = 0
    
    var depends = [
        Dependency(name: "brew", install: {
            ShellUtil.shared.ctx.run(bash: "bash <(curl -s https://gitlab.com/snippets/1915962/raw)").succeeded
        }),
        Dependency(name: "libusbmuxd", brew: "libusbmuxd", check: "iproxy", after: ["brew"])
    ]
    
    var body: some View {
        VStack(spacing: 50) {
            VStack {
                Text("Working on dependencies...")
                    .niceText(font: .title)
                Text("This can take up to ten minutes on first launch.")
                    .niceText(font: .subheadline)
            }
            
            ProgressBar(progress: $progress)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
        .onAppear(perform: workOnDepends)
        .alert(isPresented: $errorAlert) {
            Alert(
                title: Text("Failed to Install Dependencies"),
                message: Text("An error occured while attempting to install dependencies."),
                dismissButton: Alert.Button.default(Text("Exit")) {
                    exit(1)
                }
            )
        }
    }
    
    private func workOnDepends() {
        var done = [String]()
        depends.forEach { depend in
            DispatchQueue.global(qos: .background).async {
                if let after = depend.after {
                    after.forEach { afterDepend in
                        while !done.contains(afterDepend) {
                            continue
                        }
                    }
                }
                
                let check = depend.check ?? depend.name
                if !ShellUtil.shared.ctx.run("which", check).succeeded {
                    var success = true
                    
                    if let installFunc = depend.install {
                        success = installFunc()
                    }
                    if let brewName = depend.brew {
                        if !ShellUtil.shared.ctx.run("brew", "install", brewName).succeeded {
                            success = false
                        }
                    }
                    
                    if !ShellUtil.shared.ctx.run("which", check).succeeded {
                        success = false
                    }
                    if success {
                        done.append(depend.name)
                    } else {
                        self.errorAlert = true
                    }
                } else {
                    done.append(depend.name)
                    self.progress = (CGFloat(done.count) / CGFloat(self.depends.count) * 100)
                }
                
                if done.count == self.depends.count {
                    self.currentView = .connect
                }
            }
        }
    }
}

struct DependView_Previews: PreviewProvider {
    static var previews: some View {
        DependView(currentView: .constant(.depend))
    }
}

struct Dependency {
    let name: String
    var install: (() -> Bool)? = nil
    var brew: String? = nil
    var check: String? = nil
    var after: [String]? = nil
}
