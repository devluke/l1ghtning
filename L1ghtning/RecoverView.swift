//
//  RecoverView.swift
//  ReRa1n
//
//  Created by Luke Chambers on 11/15/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI

struct RecoverView: View {
    @Binding var currentView: L1ghtningView
    
    var body: some View {
        VStack(spacing: 50) {
            Text("Recovery")
                .niceText(font: .largeTitle)
            
            ActionList(columns: [
                ListColumn(actions: [
                    ListAction(label: "Exit Respring Loop", command: .safeMode)
                ], title: "Loops"),
                ListColumn(actions: [
                    ListAction(label: "Fix Cydia Crashes", command: .fixCydiaCrashes),
                    ListAction(
                        label: "Uninstall Cydia",
                        confirm: true,
                        confirmMessage: "This action will reboot your device and you'll have to rejailbreak. Are you sure you want to uninstall Cydia?",
                        command: .uninstallCydia
                    )
                ], title: "Cydia")
            ])
            
            Button(action: {
                self.currentView = .home
            }) {
                Text("Back")
                    .niceButtonText()
            }.niceButton(preset: .bottomButton)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct RecoverView_Previews: PreviewProvider {
    static var previews: some View {
        RecoverView(currentView: .constant(.recover))
    }
}
