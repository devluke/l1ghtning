//
//  ShellUtil.swift
//  L1ghtning
//
//  Created by Luke Chambers on 11/23/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import SwiftShell

class ShellUtil {
    static let shared = ShellUtil()
    private init() {
        ctx = CustomContext(main)
        ctx.env["PATH"] = "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
    }
    
    var ctx: CustomContext
}

extension String {
    func adminCommand() -> String {
        return "osascript -e 'do shell script \"\(self)\" with administrator privileges'"
    }
    
    func runCommandHackilyButSwiftly(withAdmin: Bool = false) {
        let path = ShellUtil.shared.ctx.env["PATH"] ?? ""
        NSAppleScript(source: "do shell script \"PATH=\(path) && \(self)\" \(withAdmin ? "with administrator privileges" : "")")?
            .executeAndReturnError(nil)
    }
}
