//
//  ConnectView.swift
//  ReRa1n
//
//  Created by Luke Chambers on 11/15/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import SwiftUI
import SwiftShell

struct ConnectView: View {
    @Binding var currentView: L1ghtningView
    
    @State private var method = 0
    @State private var ipAddress = ""
    @State private var password = ""
    @State private var connectAlert = false
    @State private var connectAlertMessage = ""
    @State private var secretRecovery = false
    @State private var connecting = false
    @State private var connectText = ""
    @State private var connectProgress: CGFloat = 0
    
    var body: some View {
        VStack(spacing: 50) {
            VStack {
                Text("L1ghtning")
                    .niceText(font: .largeTitle)
                Text("Let's start by getting your device connected.")
                    .niceText(font: .headline)
            }
            
            VStack {
                if !connecting {
                    Text("Would you like to connect over USB or Wi-Fi?")
                        .niceText(font: .subheadline)
                    
                    Picker(selection: $method, label: EmptyView()) {
                        Text("USB").tag(0)
                        Text("Wi-Fi").tag(1)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    
                    if (method == 1) {
                        TextField("IP Address", text: $ipAddress)
                            .niceField(placeholder: "IP Address", text: $ipAddress)
                    }
                    
                    SecureField("Root Password (default is alpine)", text: $password)
                        .niceField(placeholder: "Root Password (default is alpine)", text: $password)
                    
                    Text("This page hasn't been completely updated to the nicer UI yet.")
                        .foregroundColor(.red)
                        .niceText()
                    
                    if (method == 0 && secretRecovery) {
                        Button(action: {
                            self.currentView = .recoveryConnect
                        }) {
                            Text("Use Recovery Mode")
                                .niceButtonText()
                        }.niceButton(color: .gray)
                    }
                } else {
                    Text(connectText)
                        .niceText()
                    ProgressBar(progress: $connectProgress)
                }
            }
            
            if !connecting {
                Button(action: {
                    if self.password == "lukeisthebestdev" {
                        self.secretRecovery = true
                    } else if self.password == "iwantmydevicetodie" {
                        UtilityView.showDestruction = true
                    } else if self.password == "niceuiisnotnice" {
                        NiceUI.use.toggle()
                        self.currentView = .depend
                    } else {
                        self.connecting = true
                        self.connectText = "Creating connect thread..."
                        self.connectProgress = 0
                        
                        let usbNeeded = self.method == 0 && !Connection.shared.sshOverUsbRunning
                        
                        var currentPart = 0
                        let connectParts = usbNeeded ? 4 : 3
                        
                        func nextPart(_ label: String) {
                            currentPart += 1
                            self.connectProgress = (CGFloat(currentPart) / CGFloat(connectParts)) * 100
                            self.connectText = label
                        }
                        
                        DispatchQueue.global(qos: .background).async {
                            if usbNeeded {
                                nextPart("Starting SSH over USB...")
                                ShellUtil.shared.ctx.run(bash: "iproxy 5853 44".adminCommand())
                                Connection.shared.sshOverUsbRunning = true
                            }
                            
                            nextPart("Connecting to device...")
                            let connectIp = self.method == 0 ? "localhost" : self.ipAddress
                            let connectPort: Int32 = self.method == 0 ? 5853 : 22
                            let error = Connection.shared.connect(host: connectIp, port: connectPort, password: self.password)
                            
                            var infoError: ConnectionError?
                            if error == nil {
                                nextPart("Gathering device info...")
                                infoError = Connection.shared.startInfo()
                                
                                if infoError == nil {
                                    nextPart("Done!")
                                }
                            }
                            
                            if error == nil && infoError == nil {
                                self.currentView = .home
                            } else {
                                if error == .incorrectPassword {
                                    self.connectAlertMessage = "The password you entered is not correct."
                                } else {
                                    self.connectAlertMessage = "An error occurred while attempting to connect to the device."
                                }
                                self.connecting = false
                                self.connectAlert = true
                            }
                        }
                    }
                }) {
                    Text("Connect")
                        .niceButtonText()
                }
                .niceButton(preset: .bottomButton)
                .alert(isPresented: $connectAlert) {
                    Alert(title: Text("Failed to Connect"), message: Text(connectAlertMessage))
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding(50)
    }
}

struct ConnectView_Previews: PreviewProvider {
    static var previews: some View {
        ConnectView(currentView: .constant(.connect))
    }
}
