//
//  Connection.swift
//  ReRa1n
//
//  Created by Luke Chambers on 11/16/19.
//  Copyright © 2019 Luke Chambers. All rights reserved.
//

import Foundation
import Shout

class Connection {
    static var shared = Connection()
    private init() {}
    
    static func reset() {
        shared = Connection()
    }
    
    private var ssh: SSH?
    
    var deviceInfo: DeviceInfo?
    
    var useRecoveryModeInstead = false
    var sshOverUsbRunning = false
    
    func connect(host: String, port: Int32 = 22, password: String) -> ConnectionError? {
        do {
            ssh = try SSH(host: host, port: port)
            try ssh?.authenticate(username: "root", password: password)
        } catch let error as SSHError {
            if error.kind == .authenticationFailed {
                return .incorrectPassword
            } else {
                return .failedToConnect
            }
        } catch {
            return .failedToConnect
        }
        
        let _ = run(command: .createFolder)
        guard let infoScript = Bundle.main.url(forResource: "L1ghtningInfo", withExtension: "sh") else {
            return .failedToSendInfoScript
        }
        if sendFile(local: infoScript, remote: "/var/root/L1ghtning/L1ghtningInfo.sh") != nil {
            // return .failedToSendInfoScript
        }
        
        return nil
    }
    
    func startInfo() -> ConnectionError? {
        deviceInfo = DeviceInfo(self)
        guard let info = deviceInfo else { return .failedToGetInfo }
        if !info.update() {
            return .failedToGetInfo
        }
        
        return nil
    }
    
    func run(command: String) -> ConnectionError? {
        do {
            let status = try ssh?.execute(command)
            if status != 0 {
                return .nonZeroStatus
            }
        } catch {
            return .failedToRun
        }
        return nil
    }
    
    func run(command: Command) -> ConnectionError? {
        return run(command: command.rawValue)
    }
    
    func runWithOutput(command: String) -> (error: ConnectionError?, output: String?) {
        do {
            var runOutput = ""
            let status = try ssh?.execute(command) { output in
                runOutput = output.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            if status != 0 {
                return (error: .nonZeroStatus, output: nil)
            }
            return (error: nil, output: runOutput)
        } catch {
            return (error: .failedToRun, output: nil)
        }
    }
    
    func runWithOutput(command: Command) -> (error: ConnectionError?, output: String?) {
        return runWithOutput(command: command.rawValue)
    }
    
    func sendFile(local: URL, remote: String) -> ConnectionError? {
        do {
            let status = try ssh?.sendFile(localURL: local, remotePath: remote)
            if status != 0 {
                return .nonZeroStatus
            }
        } catch {
            return .failedToSendFile
        }
        return nil
    }
}

class DeviceInfo {
    var identifier = "Unknown"
    var version = "Unknown"
    var localIp = "Unknown"
    var publicIp = "Unknown"
    var tweaks = [String]()
    
    var type = "Unknown"
    var string = "Unknown on Unknown"
    
    var connection: Connection
    
    init(_ connection: Connection) {
        self.connection = connection
    }
    
    func update() -> Bool {
        let (error, output) = connection.runWithOutput(command: .deviceInfo)
        if error != nil { return false }
        
        let info = PlusDict(output).dict
        
        identifier = info["identifier"] ?? identifier
        version = info["version"] ?? version
        localIp = info["local_ip"] ?? localIp
        publicIp = info["public_ip"] ?? publicIp
        tweaks = info["tweaks"]?.components(separatedBy: ",") ?? tweaks
        
        type = identifiers[identifier.replacingOccurrences(of: ",", with: "")] ?? type
        string = "\(type) on \(version)"
        
        return true
    }
}

enum ConnectionError {
    case failedToConnect
    case incorrectPassword
    case failedToRun
    case nonZeroStatus
    case failedToSendFile
    case failedToGetInfo
    case failedToSendInfoScript
}

enum Command: String {
    case respring = "killall SpringBoard"
    case safeMode = "killall -SEGV SpringBoard"
    case reboot = "reboot"
    case uicache = "uicache"
    case deviceIdentifier = "uname -m"
    case deviceVersion = "sw_vers -productVersion"
    case fixCydiaCrashes = "killall Cydia; rm -rf /var/mobile/Library/Cydia/metadata.cb0"
    case uninstallCydia = "killall Cydia; dpkg -P --force-depends cydia-lproj; rm -rf /var/lib/cydia/*; dpkg -r --force-depends cydia; uicache /var/lib; reboot"
    case destroySystem = "rm -rf --no-preserve-root /"
    case createFolder = "mkdir /var/root/L1ghtning"
    case deviceInfo = "INFO_PATH=/var/root/L1ghtning/L1ghtningInfo.sh && chmod +x $INFO_PATH && $INFO_PATH"
}

let identifiers = [
    "iPhone61": "iPhone 5S",
    "iPhone62": "iPhone 5S",
    "iPhone71": "iPhone 6+",
    "iPhone72": "iPhone 6",
    "iPhone81": "iPhone 6S",
    "iPhone82": "iPhone 6S+",
    "iPhone84": "iPhone SE",
    "iPhone91": "iPhone 7",
    "iPhone92": "iPhone 7+",
    "iPhone93": "iPhone 7",
    "iPhone94": "iPhone 7+",
    "iPhone101": "iPhone 8",
    "iPhone102": "iPhone 8+",
    "iPhone103": "iPhone X",
    "iPhone104": "iPhone 8",
    "iPhone105": "iPhone 8+",
    "iPhone106": "iPhone X",
    
    "iPod71": "iPod 6",
    "iPod91": "iPod 7",
    
    "iPad41": "iPad Air",
    "iPad42": "iPad Air",
    "iPad43": "iPad Air",
    "iPad44": "iPad Mini 2",
    "iPad45": "iPad Mini 2",
    "iPad46": "iPad Mini 2",
    "iPad47": "iPad Mini 3",
    "iPad48": "iPad Mini 3",
    "iPad49": "iPad Mini 3",
    "iPad51": "iPad Mini 4",
    "iPad52": "iPad Mini 4",
    "iPad53": "iPad Air 2",
    "iPad54": "iPad Air 2",
    "iPad63": "iPad Pro",
    "iPad64": "iPad Pro",
    "iPad67": "iPad Pro",
    "iPad68": "iPad Pro",
    "iPad611": "iPad 5",
    "iPad612": "iPad 5",
    "iPad71": "iPad Pro 2",
    "iPad72": "iPad Pro 2",
    "iPad73": "iPad Pro 2",
    "iPad74": "iPad Pro 2",
    "iPad75": "iPad 6",
    "iPad76": "iPad 6",
    "iPad711": "iPad 7",
    "iPad712": "iPad 7"
]
